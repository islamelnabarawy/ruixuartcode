function a=degrade(I,noise)

if noise<0
  Error('Noise must be between 0 and 1')
end
if noise>1
  Error('Noise must be between 0 and 1')
end
[n np]=size(I);
for i=1:n
  for j=1:np
    if rand(1,1)<noise
        a(i,j)=round(rand(1,1));
    else
      a(i,j)=I(i,j);
    end
  end
end
