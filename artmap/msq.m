function x=msq(n1,n2,I)

  if size(I) ~= n1*n2
    Error('Error in "msq.m"');
  end

  x='';
  for i=1:n1
    x=[x,I((i-1)*n2+1:i*n2,1)];
  end
