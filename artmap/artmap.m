function artmap(rhobar,L,noise)

epsilon=0.0001;
no=100;
n=200;
np=18;
Mb=3;
M=1;
Io=initI;
Ic=1-Io;
I=[Io;Ic];
b=initb;
z=ones(n,1);
w=ones(Mb,1);
zold=z;
wold=w;
learning=1;
iterations=0;
while learning==1
  iterations=iterations+1;
  for pattern=1:np
    rho=rhobar;
    ok=0;
    for j=1:M
      T(j)=norm(min(I(:,pattern),z(:,j)),1)/(L-1+norm(z(:,j),1));
    end
    while ok==0
      [maxT,J]=max(T);
      if rho*norm(I(:,pattern),1) <= norm(min(I(:,pattern),z(:,J)),1)
        [maxW,K]=max(b(:,pattern));
        if w(K,J) == 1
          ok=1;
        else
          rho=norm(min(I(:,pattern),z(:,J)),1)/norm(I(:,pattern),1)+epsilon;
          T(J) = -1;
          ok=0;
        end
      else
        T(J) = -1;
      end
    end
    z(:,J)=min(I(:,pattern),z(:,J));
    w(:,J)=min(b(:,pattern),w(:,J));
    if J==M
      M=M+1;
      z=[z ones(n,1)];
      w=[w ones(Mb,1)];
    end
  end
  if size(z)==size(zold)
    if z==zold
      if size(w)==size(wold)
        if w==wold
          learning=0;
        end
      end
    end
  end
  zold=z;
  wold=w;
end

iterations
categories=M
test_artmap(Io,z,w,rhobar,L,10,10,noise);
