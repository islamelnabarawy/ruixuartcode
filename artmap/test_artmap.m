function test_artmap(Io,z,w,rhobar,L,n1,n2,noise)

Io=degrade(Io,noise);
Ic=1-Io;
I=[Io;Ic];
rho=rhobar;
[n np]=size(I);
[Mb M]=size(w);
  for pattern=1:np
    ok=0;
    for j=1:M
      T(j)=norm(min(I(:,pattern),z(:,j)),1)/(L-1+norm(z(:,j),1));
    end
    while ok==0
      [maxT,J]=max(T);
      if rho*norm(I(:,pattern),1) <= norm(min(I(:,pattern),z(:,J)),1)
        ok=1;
      else
        T(J) = -1;
      end
    end
    [maxW,K]=max(w(:,J));
    clf
    art1plot(msq(n1,n2,Io(:,pattern))');
    if J==M
      ss=sprintf('Pattern Number: %d, Predicted Class is: Do not know',pattern);
      text(0,0,ss)
    else
      ss=sprintf('Pattern Number: %d, Predicted Class is: %d',pattern,K);
      text(0,0,ss)
    end
    pause
  end
