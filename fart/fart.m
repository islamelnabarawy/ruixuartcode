function fart(rho,alpha)

aa=fuzzy(rho,alpha);
load wij;
nc=size(wij);
ncat=nc(1)-1;
load points;
np=size(points);
npunt=np(1);

sym=sprintf('.ox*sdv^<>ph');
ns=size(sym);
nsym=ns(2);
col=sprintf('ymcrgbwk');

clf
h=gcf;
set(h,'name','Fuzzy ART');
hold;
for cnp=1:1:npunt
  if aa(cnp) < 13
   plot(points(cnp,1),points(cnp,2),sym(aa(cnp)));
  else
   plot(points(cnp,1),points(cnp,2),'x');
  end
end
for cc=1:1:ncat
   rectang(wij(cc,1),wij(cc,3),1-wij(cc,2),1-wij(cc,4));
end
hold;
axis('equal');
