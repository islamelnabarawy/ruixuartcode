function genpun(np,nc)

clusx=rand(nc,1);
clusy=rand(nc,1);
figure(1)
for point=1:1:np
   ac=rand(1);
   dcx=0.1*randn(1);
   dcy=0.1*randn(1);
   for cc=1:1:nc
      if ac< cc/nc
         if ac>(cc-1)/nc
            punt(point,1)=clusx(cc,1)+dcx;
            punt(point,2)=clusy(cc,1)+dcy;
         end
      end
   end
end
ma=max(max(punt));
mi=min(min(punt));
punt=(punt-mi)/(ma-mi);
clusx=(clusx-mi)/(ma-mi);
clusy=(clusy-mi)/(ma-mi);
plot(clusx,clusy,'+')
save points punt -ascii
hold
plot(punt(:,1),punt(:,2),'o')
hold