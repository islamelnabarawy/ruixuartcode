function map=fuzzy(rho,alpha)

F1=4;
CAT=0;

wij = ones(1,F1);
wijold=1;

load points;
ns=size(points);
np=ns(1);

niter=1
while 1>0,
 for point=1:1:np
    a(1)=points(point,1);
    a(2)=1-points(point,1);
    a(3)=points(point,2);
    a(4)=1-points(point,2);
    
    for j=1:CAT+1
      T(j) = norm(min(a,wij(j,:)),1)/(alpha+norm(wij(j,:),1));
    end
    while 1>0,
          [Tmax,Jmax]=max(T);
        if norm(min(a,wij(Jmax,:)),1) >= rho*norm(a,1)
           map(point)=Jmax;
           break;
        end
        T(Jmax)=0;
    end
    wij(Jmax,:)=min(a,wij(Jmax,:));
    if Jmax==CAT+1
      CAT=CAT+1;
      wij = aug(wij,1);
   end
end
if size(wij)==size(wijold)
  if wij == wijold
        break;
  end
end
wijold=wij;
ss=sprintf('niter=%d CAT=%d',niter,CAT)
niter = niter+1;
end
save wij wij -ascii
save map map -ascii
