function aa=aug(a,n)

[lines cols]=size(a);
u=ones(1,cols);
for i=1:n
  a = [a;u];
end
aa = a;