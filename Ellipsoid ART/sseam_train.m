%SSEAM_TRAIN
%
%   SSEAM_TRAIN.DLL ver 2.0
%   The DLL implements slow (fast-commit/slow-recode) & fast learning for 
%   the training phase of semi-supervised Ellipsoid-ARTMAP (ssEAM) classifier
%   using Weber Law category choice function. and fast commit/slow recode learning.
%   To use it as the original ssEAM training algorithm, set D=sqrt(M)/mu and
%   normalize your input patterns so that their components lie inside the same
%   range of values. You may also use SSEAM_TRAIN to perform semi-supervised
%   Ellipsoid-ART training (clustering rather than classification) on data, if you
%   attribute each pattern with the same label. When mu=1.0 ssEAM training becomes
%   semi-supervised Hypersphere-ARTMAP (ssHAM) training. Also ssEAM can be used as
%   EAM or Ellipsoid ProbART, when the value of the ssEAM tolerance parameter is set
%   to 0.0 or 1.0 respectively.
%
%
%
%   CALLING SYNTAX:
%
%   [Templates,NLabels,list_presentations] = sseam_train(Patterns,PLabels, ...
%             mu,D,vigilance,alpha,omega,tol,learning_rate, ...
%             max_list_presentations,trim_option,unknown_label,oflag);
%
%
%
%   INPUT ARGUMENTS:
%
%   Patterns:	   PxM matrix containing the training patterns in rows.
%   PLabels:	   Px1 vector containing the labels for each pattern.
%   mu:           scalar fixed ellipsoid's axis length ratio. For any
%                 ellipsoid each of its minor axes has a length of ratio
%                 times the length of the ellipsoid's major axis. It
%                 takes values in (0.0, 1.0]. If mu=1.0 then ssEAM becomes
%                 ssHAM.
%   D:            scalar feature input space L2-based diameter (>=0.0).
%                 In original ssEAM, D=sqrt(M)/mu, where M is the
%                 dimensionality of the feature input space patterns.
%                 Note that for the original ssEAM, input patterns must
%                 have all of their components .
%   vigilance:	   scalar baseline vigilance parameter in [0.0, 1.0].
%   alpha:        scalar choice parameter. Must be >0.
%   omega:        omega parameter of ssEAM, which plays a similar role
%                 to the initial template component value wu for
%                 uncommitted nodes in FAM. Should be >=1.0.
%   tol:          scalar tolerance parameter of ssEAM taking values in [0,1].
%                 When tol=0.0, ssEAM behaves like EAM. When tol=1.0, ssEAM
%                 behaves like Ellipsoid ProbART.
%   learning_rate:scalar learning parameter in (0.0, 1.0]. If equal to 1.0,
%                 fast learning is performed; for any other values slow
%                 learning (fast-commit/slow-recode) takes place.
%   max_list_presentations: scalar maximum number (>=1) of list presentations
%                 (epochs). max_list_presentations=1 corresponds to on-line
%                 learning.
%   trim_option:  scalar option flag {0=NO,1=YES} to perform node trimming
%                 after ssEAM has converged. Trimming refers to the removal
%                 of inactive nodes, that is, nodes that are not selected by
%                 any of the training patterns after ssEAM has converged.
%                 It has been implemented only for the case where FAMawd has
%                 converged after fast learning.
%   unknown_label:scalar integer label to be used for categories that
%                  have no dominant class label.
%   oflag:	       scalar option flag {0,1,2,3,4} that controls the output
%                  on the screen regarding the training phase. If oflag=0
%                  then no output is generated. 
%
%
%
%   RETURN ARGUMENTS:
%
%   Templates:    Nx(2M+1) matrix containing the templates in rows. N is
%                 the number of nodes (categories) created through
%                 training. Each template is of the form w=[Mu dVector R]
%                 where Mu is the 1xM vector representing the center of
%                 the category, dVector is the category's 1xM direction
%                 vector and R is the category's radius.
%   NLabels:	   Nx1 vector containing the node labels.
%   list_presentations: scalar number of list presentations performed. Note
%                 that for learning_rate=1.0 (fast learning), if FAMawd
%                 is trained to completion, the last list presentation,
%                 which confirms convergence, is not accounted for.
%
%
%
%   NOTES: 
%   
%   All vectors are column vectors.
%   M is the dimension of the input patterns.
%   P is the number of training patterns.
%   N is the number of categories created after training.
%
%   1) Use of this DLL is restricted as specified in the Terms Of Use
%   section www.geocities.com/g_anagnostop. You may contact the author
%   for more information.
%
%
%
%   Authored by
%     Georgio Anagnostopoulos          
%     anagnostop@email.com             
%     November 2001 (c).
%