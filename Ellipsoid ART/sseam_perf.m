% SSEAM_PERF
%
%   SSEAM_PERF.DLL ver 2.0
%   The DLL implements the performance phase of semi-supervised Ellipsoid-ARTMAP
%   (ssEAM) classifier using Weber Law category choice function. To use it as the
%   original ssEAM performance algorithm, set D=sqrt(M)/mu and normalize your
%   input patterns (their components must lie in the same range).
%
%
%
%   CALLING SYNTAX:
%
%   [PLabels, CCFvalues, CMFvalues] = sseam_perf(Templates,NLabels,mu,D, ...
%            vigilance, alpha,omega,Patterns,unknown_label,ignore_option,oflag);
%
%
%   INPUT ARGUMENTS:
%
%   Templates:    Nx(2M+1) matrix containing the templates in rows. N is
%                 the number of nodes (categories). Also, each template
%                 is of the form w=[Mu dVector R] where Mu is the 1xM
%                 vector representing the center of the category,
%                 dVector is the category's 1xM direction vector and R
%                 is the category's radius.
%   NLabels:      Nx1 vector containing the node (category) integer labels.
%   mu:           scalar fixed ellipsoid's axis length ratio. For any
%                 ellipsoid each of its minor axes has a length of ratio
%                 times the length of the ellipsoid's major axis. It
%                 takes values in (0.0, 1.0]. You should use the same
%                 value of mu as the one you have used for training.
%   D:            scalar feature input space L2-based diameter (>=0.0).
%                 In original EAM, D=sqrt(M)/mu, where M is the
%                 dimensionality of the feature input space patterns.
%                 You should use the same value of D as the one you have
%                 used for training.
%   vigilance:    scalar vigilance parameter with values in
%                 [0.0, 1.0].
%   alpha:        scalar choice parameter. Must be >0.
%   omega:        omega parameter of ssEAM, which plays a similar role
%                 to the initial template component value wu for
%                 uncommitted nodes in FAM. Should be >=1.0.
%   Patterns:     PxM matrix contains P testing patterns of
%                 dimensionality M in rows.
%   unknown_label:scalar integer label to be used for patterns that
%                 cannot be classified.
%   ignore_option:scalar equal to 0 or 1. Not yet implemented.
%   oflag:        scalar option flag {0,1,2,3} that controls the output
%                 on the screen regarding the training phase. If oflag=0
%                 then no output is generated. 
%
%
%
%   RETURN ARGUMENTS:
%
%   PLabels:      Px1 vector containing the integer label for each testing
%                 pattern.
%   CCFvalues:    PxN matrix containing the CCF values for each test pattern
%                 per node (category). A value of -1 indicates a node that 
%                 was reset because of failing to pass either the Vigilance
%                 Test or the Commitment Test.
%   CMFvalues:    PxN matrix containing the CMF values for each test pattern
%                 per node (category).
%
%
%
%   NOTES: 
%   
%   All vectors are column vectors.
%   M is the dimension of the input patterns.
%   P is the number of training patterns.
%   N is the number of nodes created after training.
%
%   1) The performance phase of the ssEAM classifier is identical to the one
%   of the EAM classifier. The reason you are provided with SSEAM_PERF is that
%   the DLL additionally accepts omega and ignore_flag as input arguments.
%
%   2) To force classification of all patterns during a performance phase, set
%   vigilance=0.0 and omega=inf.
%
%   3) SSEAM_PERF promptly updates the MATLAB's FLOPS counter by taking
%   into account all flops and all comps (comparison operations between
%   floating point numbers) performed.
%
%   4) Use of this DLL is restricted as specified in the Terms Of Use section
%   at www.geocities.com/g_anagnostop. You may contact the author for more
%   information.
%
%
%
%   Authored by
%     Georgio Anagnostopoulos          
%     anagnostop@email.com             
%     June 2003 (c).
%