function weight=gen_uncommit_node(ori_weight)
%
%Generate an uncommitted node
%
%Input variables:
%ori_weight: the original weight matrix MxN 
%
%Output variables:
%weight: the augumented weight matrix (M+1)xN
%
%w_new=gen_uncommit_node(w_old)


[lines cols]=size(ori_weight);

%Generate a 1xN vector
v=ones(1,cols); 

%augument the original weight matrix by adding a 1xN vector
ori_weight = [ori_weight;v];

weight = ori_weight;