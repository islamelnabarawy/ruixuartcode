function varargout=combinatorial(m,n)

if n>=m-n
    max_num=n;
else
    max_num=m-n;
end

if n<=m-n
    min_num=n;
else
    min_num=m-n;
end

com=1;

i=m;
while i>max_num
    com=com*i;
    i=i-1;
end

i=2;
while i<=min_num
    com=com/i;
    i=i+1;
end

varargout{1}=com;

