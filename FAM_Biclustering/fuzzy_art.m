function varargout=fuzzy_art(data, rho, alpha, beta, max_epoches, prune_option)

%Fuzzy ART
% This function implements the Fuzzy ART by G. Carpenter, S. Grossberg, and D. Rosen, ��Fuzzy ART: fast stable learning and categorization 
% of analog patterns by an adaptive resonance system,�� Neural Networks, vol. 4, pp. 759-771, 1991.
% This function takes a set of input patterns and returns the network weights and the number of clusters
%
%
% CALLING SYNTAX:
% [wij, cluster, num_of_clusters]=fuzzy_art(data, rho, alpha, beta, max_epoches, prune_option)
%
%
% INPUT VARIABLES:
%   data: training data, each row representing a sample and each column
%   representing a feature. Data are needed to be in [0.0 1.0]]
%   rho: vigilance parameter of fuzzy ART in [0.0 1.0]
%   alpha: signal rule parameter. Must be > 0.0
%   beta: learnng rate of weights of fuzzy ART. if equal to
%   1.0, fast learning is performed; for any other values, slow learning is performed
%   max_epoches: maximum number of training epoches
%   prune_option: scalar option flag to perform prunning operation, which
%   detects inactive nodes that are not selected by any of the training
%   patterns and removes them. 0 = No, 1 = Yes
%
%
% RETURN VARIABLES:
%   wij: weights of fuzzy ART
%   cluster: clusters of the data
%   num_of_clusters: number of categories obtained in layer F2 of fuzzy ART
%   
%

[num_of_samples, dim]=size(data); % calculate the number of samples and dimensions of data
num_of_clusters=0;        % number of clusters generated in Layer F2 of fuzzy ART
num_of_inputs=dim*2;      % number of inputs to fuzzy ART     

wij = ones(1,num_of_inputs);  % initialize weights of fuzzy ART

wijold=1;

niter=1; % initialize the number of iterations

disp('Clustering of Fuzzy ART begins ...');

while 1>0 & max_epoches>niter,
        for nt=1:num_of_samples;
            % generate the input to fuzzy ART based on complementary coding
            for j=1:dim
                 input(2*j-1)=data(nt,j);
                 input(2*j)=1-data(nt,j);
            end
                      
            % calculate the values of category choice function (CCF) for the
            % nodes in layer F2 of fuzzy ART
            for j=1:num_of_clusters+1
                 cat_choice(j) = norm(min(input,wij(j,:)),1)/(alpha+norm(wij(j,:),1));
            end
            
            while 1>0,
                  [Tmax,Jmax]=max(cat_choice);  % find the winning node with the maximum CCF value
                        
                  % vigilance test
                  if norm(min(input,wij(Jmax,:)),1) >= rho*norm(input,1)
                      cluster(nt)=Jmax;
                      break;
                  end
                  cat_choice(Jmax)=0; % reset the winning node
            end
             
            
            % update weights
            wij(Jmax,:)=min(input,wij(Jmax,:))*beta+(1-beta)*wij(Jmax,:);
            
            % generate a new uncomitted node, if the uncommitted code wins
            if Jmax==num_of_clusters+1
                num_of_clusters=num_of_clusters+1;
                wij = gen_uncommit_node(wij);
            end

        end %for
        
        % training ends if there is no change of the weights
        if size(wij)==size(wijold)
            if wij == wijold
                break;
            end
        end
         
        % store the old weights
        wijold=wij;
        
        niter = niter+1; % increase the number of iteration by 1
end

%Pruning: detect committed nodes that do not win for any input pattern
if prune_option==1
    cflag=zeros(1,num_of_clusters+1); % initialize the flag variable that indicates whether the nodes are used or not
    for nt=1:num_of_samples;
        % generate the input to fuzzy ART based on complementary coding
        for j=1:dim
             input(2*j-1)=data(nt,j);
             input(2*j)=1-data(nt,j);
        end  
     
        % calculate the values of category choice function (CCF) for the
        % nodes in layer F2 of fuzzy ART
        for j=1:num_of_clusters+1
            cat_choice(j) = norm(min(input,wij(j,:)),1)/(alpha+norm(wij(j,:),1));
        end
     
        while 1>0,
              [Tmax,Jmax]=max(cat_choice); % find the winning node with the maximum CCF value
              % vigilance test
              if norm(min(input,wij(Jmax,:)),1) >= rhoa*norm(input,1) 
                  cluster(nt)=Jmax;
                  break;
              end
              cat_choice(Jmax)=0; % reset the winning node
        end
          
    
         cat_choice(Jmax)=0; % reset the winning node
         cflag(Jmax)=1; % set the flag variable to 1 if nodes are active
    end
    
    % remove the detected inactive nodes
    wij_temp=[];
    for i=1:num_of_clusters
        if cflag(i)==1
            wij_temp=[wij_temp;wij(i,:)];
        end
    end   
    wij=wij_temp;
    wij = gen_uncommit_node(wij);

    number_of_active_nodes=sum(cflag(1:num_of_clusters));
end

              
% output training summary  
str=['clustering with Fuzzy ART is complete in ', num2str(niter), ' epochs'];
disp(str);
str=[num2str(num_of_clusters), ' nodes created'];
disp(str);
if prune_option==1
    str=[num2str(num_of_clusters-number_of_active_nodes),' inacitve nodes detected'];
    disp(str);
    disp(' ');
end

% output
if prune_option==1
    varargout{1}=wij;
    varargout{2}=cluster;
    varargout{3}=number_of_active_nodes;
else
    varargout{1}=wij;
    varargout{2}=cluster;
    varargout{3}=num_of_clusters; 
end

