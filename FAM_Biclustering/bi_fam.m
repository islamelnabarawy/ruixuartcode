% leukemia data set
clear all;


load processed_data;
NUM_CLASSES=3;
input_sample=input_gene';

alpha_gene=0.001;
prune_option_gene=0;
beta_gene=1.0;
max_epoches_gene=100;

best=0;
best_adj=0;

rho_gene=0.15;
  
[wij_gene, cluster_gene, num_of_clusters_gene]=fuzzy_art(input_gene, rho_gene, alpha_gene, beta_gene, max_epoches_gene, prune_option_gene);


num_of_clusters_gene
mean_gene_cluster=zeros(NUM_SAMPLE,num_of_clusters_gene);
num_gene_cluster=zeros(1,num_of_clusters_gene);
for i=1:NUM_GENE
    num_gene_cluster(cluster_gene(i))=num_gene_cluster(cluster_gene(i))+1;
    for j=1:NUM_SAMPLE
        mean_gene_cluster(j,cluster_gene(i))=mean_gene_cluster(j,cluster_gene(i))+input_sample(j,i);
    end
end
for j=1:num_of_clusters_gene
    mean_gene_cluster(:,j)=mean_gene_cluster(:,j)/num_gene_cluster(j);
end

gene_cluster=zeros(num_of_clusters_gene,max(num_gene_cluster));
count=ones(1,num_of_clusters_gene);
for i=1:NUM_GENE
    gene_cluster(cluster_gene(i),count(cluster_gene(i)))=i;
    count(cluster_gene(i))=count(cluster_gene(i))+1;
end
    
correlation_threshold=0.3;

rho_sample=0.35;

    alpha_sample=0.001;
    prune_option_sample=0;
    beta_sample=1.0;
    max_epoches_sample=100;
    wij_sample = ones(1,NUM_GENE*2);
    num_of_clusters_sample=0;
    step=0.05;

for i=1:NUM_SAMPLE
    str=['Processing sample #', num2str(i)];
    disp(str);
    rho_init=rho_sample;
    vg=0;

    while vg==0
        [wij_sample_save, cluster_sample_save, num_of_clusters_sample_save]=fuzzy_art_v2(input_sample(i,:), rho_init, alpha_sample, beta_sample, ...
            max_epoches_sample, prune_option_sample,wij_sample,num_of_clusters_sample);
        cluster_sample(i)=cluster_sample_save;
        
        %assign the sample to an existing cluster
        if num_of_clusters_sample==num_of_clusters_sample_save %if the sample is included into an existing cluster
            num_cor=0;
            for j=1:i-1
                if cluster_sample(i)==cluster_sample(j)
                    num_cor=num_cor+1;
                    for k=1:num_of_clusters_gene
                        numerator=0;
                        sum1=0;
                        sum2=0;
                        m=1;
                        while m<=max(num_gene_cluster)&& gene_cluster(k,m)>0 && num_gene_cluster(k)>1
                            numerator=numerator+(input_sample(i,gene_cluster(k,m))-mean_gene_cluster(i,k))*(input_sample(j,gene_cluster(k,m))-mean_gene_cluster(j,k));
                            sum1=sum1+(input_sample(i,gene_cluster(k,m))*input_sample(i,gene_cluster(k,m)));
                            sum2=sum2+(input_sample(j,gene_cluster(k,m))*input_sample(j,gene_cluster(k,m)));
                            m=m+1;
                        end
                        correlation(num_cor,k)=numerator/(sqrt(sum1)*sqrt(sum2));
                    end
                end
            end
            
            correlation_mean=mean(correlation,1);
            cor_flag=0;
            for k=1:num_of_clusters_gene
                if abs(correlation_mean(k))>correlation_threshold
                    cor_flag=1;
                    break;
                end
            end
            if cor_flag==1
                disp('Passed');
                wij_sample=wij_sample_save;
                %cluster_sample=cluster_sample_save; 
                num_of_clusters_sample=num_of_clusters_sample_save;
                vg=1;
            else
                rho_init=rho_init+step;
                if rho_init>1
                    rho_init=1;
                end
            end
            clear correlation  correlation_mean;
            
        else
            % new cluster created; always allow new clusters
            wij_sample=wij_sample_save;
           % cluster_sample=cluster_sample_save; 
            num_of_clusters_sample=num_of_clusters_sample_save;
            vg=1;
        end
     end %while
end %for sample

ar=0;
br=0;
cr=0;
dr=0;
for i=1:NUM_SAMPLE
    for j=i+1:NUM_SAMPLE
        if nlabel(i)==nlabel(j) && cluster_sample(i)==cluster_sample(j)
            ar=ar+1;
        elseif nlabel(i)~=nlabel(j) && cluster_sample(i)==cluster_sample(j)
            br=br+1;
        elseif nlabel(i)==nlabel(j) && cluster_sample(i)~=cluster_sample(j)
            cr=cr+1;
        elseif nlabel(i)~=nlabel(j) && cluster_sample(i)~=cluster_sample(j)
            dr=dr+1;
        end
    end
end

rand=(ar+dr)/(ar+br+cr+dr)
if rand>best
    best=rand;
end

u=zeros(1,NUM_CLASSES);
for i=1:NUM_SAMPLE
    u(nlabel(i))=u(nlabel(i))+1;
end

v=zeros(1,num_of_clusters_sample);
for i=1:NUM_SAMPLE
    v(cluster_sample(i))=v(cluster_sample(i))+1;
end

uv=zeros(NUM_CLASSES,num_of_clusters_sample);
for i=1:NUM_SAMPLE
    uv(nlabel(i),cluster_sample(i))=uv(nlabel(i),cluster_sample(i))+1;
end

sum_of_u=0;
sum_of_v=0;
sum_of_uv=0;

for i=1:NUM_CLASSES
    if u(i)>1
        sum_of_u=sum_of_u+combinatorial(u(i),2);
    end
end

for i=1:num_of_clusters_sample
    if v(i)>1
        sum_of_v=sum_of_v+combinatorial(v(i),2);
    end
end

for i=1:NUM_CLASSES
    for j=1:num_of_clusters_sample
        if uv(i,j)>1
            sum_of_uv=sum_of_uv+combinatorial(uv(i,j),2);
        end
    end
end

adj_rand=(sum_of_uv-(sum_of_u*sum_of_v)/combinatorial(NUM_SAMPLE,2))/(0.5*(sum_of_u+sum_of_v)-(sum_of_u*sum_of_v)/combinatorial(NUM_SAMPLE,2))
if best_adj<adj_rand
    best_adj=adj_rand;
end



[fid,message]=fopen('C:/Documents and Settings/rxu/My Documents/MATLAB/FAM_Biclustering/leukemia14.data','at');

fprintf(fid,'Number of gene clusters is %d, with vigilance parameter set at %f\n',num_of_clusters_gene, rho_gene);
fprintf(fid,'Number of sample clusters is %d, with vigilance parameter set at %f \n',num_of_clusters_sample,rho_sample);
fprintf(fid,'The correlation threshold is %f \n',correlation_threshold);
fprintf(fid,'The rand index is %f\n',rand);
fprintf(fid,'The adjusted rand index is %f \n',adj_rand);

fprintf(fid,'The clusters generated are \n');
for i=1:NUM_SAMPLE
   fprintf(fid,'%d,',cluster_sample(i));
   if i==13 || i==22 || i==34
       fprintf(fid,'\n');
   end
end
fprintf(fid,'\n');

fclose(fid);

