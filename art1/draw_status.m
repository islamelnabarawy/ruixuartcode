function draw_status(M,Mold,pattern,np,J,Jold,I,z,zold,n1,n2,first,iter)

  clf

  subplot(2,M+2,M+2+1)
  axis('equal')
  axis off
  x='';
  for(i=1:n1)
    x=[x,I(n2*(i-1)+1:n2*i,pattern)];
  end
  art1plot(x);
  ss=sprintf('Iteration: %d,  Pattern: %d',iter,pattern);
  text(0,-0.5,ss)
  for(j=1:M)
    subplot(2,M+2,M+2+j+2)
    x='';
    for i=1:n1
      x=[x,z(n2*(i-1)+1:n2*i,j)];
    end
    art1plot(x);
    if j==J
      hold on
      plot([0.5,0.5],[0.5,n2+0.5],'y')
      plot([0.5,n1+0.5],[n2+0.5,n2+0.5],'y')
      plot([n1+0.5,n1+0.5],[n2+0.5,0.5],'y')
      plot([n1+0.5,0.5],[0.5,0.5],'y')
      hold off
    end
  end

  if first==1
    break;
  end

  pold=pattern-1;
  if pold==0
    pold=np;
  end
  subplot(2,M+2,1)
  axis('equal')
  axis off
  x='';
  for(i=1:n1)
    x=[x,I(n2*(i-1)+1:n2*i,pold)];
  end
  art1plot(x);
  for(j=1:Mold)
    subplot(2,M+2,j+2)
    x='';
    for i=1:n1
      x=[x,zold(n2*(i-1)+1:n2*i,j)];
    end
    art1plot(x);
    if j==Jold
      hold on
      plot([0.5,0.5],[0.5,n2+0.5],'y')
      plot([0.5,n1+0.5],[n2+0.5,n2+0.5],'y')
      plot([n1+0.5,n1+0.5],[n2+0.5,0.5],'y')
      plot([n1+0.5,0.5],[0.5,0.5],'y')
      hold off
    end
  end

