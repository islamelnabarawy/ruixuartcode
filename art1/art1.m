%function art(n1,n2,rho,L,np)
clear all;
rho=0.5;
L=1.2;
np=3;

clf
n=5;%n1*n2;
z=ones(n,1);
zold=z;
zprev=z;
Jold=1;
Mold=1;
M=1;
first=1;
I=[1 1 1 0 0;
    1 0 1 1 0;1 1 0 1 0;
        ]';
% I='';
% for pattern=1:np
%   I=[I,round(rand(n,1))];
% end
iter=0;
learning=1;
while learning==1
  iter=iter+1;
  for pattern=1:np
    ok=0;
    for j=1:M
      T(j)=norm(min(I(:,pattern),z(:,j)),1)/(L-1+norm(z(:,j),1));
    end
    while ok==0
      [maxT,J]=max(T);
      if rho*norm(I(:,pattern),1) <= norm(min(I(:,pattern),z(:,J)),1)
        ok=1;
      else
        T(J) = -1;
      end
    end
    z(:,J)=min(I(:,pattern),z(:,J));
    if J==M
      M=M+1;
      z=[z ones(n,1)];
    end
%     draw_status(M,Mold,pattern,np,J,Jold,I,z,zprev,n1,n2,first,iter)
    first=0;
    Jold=J;
    Mold=M;
    zprev=z;
%     pause
  end
  if size(z)==size(zold)
    if z==zold
      learning=0;
    end
  end
  zold=z;
end
