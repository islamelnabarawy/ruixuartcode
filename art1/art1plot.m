function art1plot(x)

[n,m]=size(x);
x=(1-x)*100;
image(x)
axis('off')
axis('equal')
axis([0.5 m+0.5 0.5 n+0.5])
colormap('gray')
